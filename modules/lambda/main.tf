data "archive_file" "lambda_zip" {
    type          = "zip"
    source_file   = "./project/${var.project_name}.py"
    output_path   = "${var.project_name}.zip"
}

data "aws_iam_policy_document" "AWSLambdaTrustPolicy" {
  statement {
    actions    = ["sts:AssumeRole"]
    effect     = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "my_lambda_role" {
  name               = "${var.project_name}-lambda-role"
  assume_role_policy = "${data.aws_iam_policy_document.AWSLambdaTrustPolicy.json}"
}

resource "aws_iam_policy" "my_policy" {
  name        = "${var.project_name}-policy"
  description = "A ${var.project_name} policy"

  policy = var.policy
}

resource "aws_iam_role_policy_attachment" "my_lambda_policy" {
  role       = "${aws_iam_role.my_lambda_role.name}"
  policy_arn = aws_iam_policy.my_policy.arn
}

resource "aws_lambda_function" "function_lambda" {
  filename         = "${var.project_name}.zip"
  function_name    = "${var.project_name}"
  role             = "${aws_iam_role.my_lambda_role.arn}"
  handler          = "${var.project_name}.lambda_handler"
#  handler          = "lambda_function.lambda_handler"
  source_code_hash = "${data.archive_file.lambda_zip.output_base64sha256}"
  runtime          = var.runtime
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda" {
    statement_id = "AllowExecution"
    action = "lambda:InvokeFunction"
    function_name = "${aws_lambda_function.function_lambda.function_name}"
    principal = "events.amazonaws.com"
    source_arn = var.lambda_trigger
}

resource "aws_lambda_function_event_invoke_config" "test" {
  function_name = aws_lambda_function.function_lambda.function_name

  destination_config {
    on_success {
      destination = var.lambda_destination
    }
  }
}