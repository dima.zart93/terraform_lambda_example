resource "aws_vpc" "vpc" {
  cidr_block = var.cidr_block

    tags = var.tags
}

resource "aws_subnet" "vpc_subnet" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.network
  availability_zone = var.az
  map_public_ip_on_launch = var.public_access
  tags = var.tags
}

resource "aws_network_interface" "vpc_nic" {
  subnet_id   = aws_subnet.vpc_subnet.id
  private_ips = var.ip_address

  tags = var.tags
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = var.tags
}

resource "aws_default_route_table" "vpc_routes" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = var.tags
}  

resource "aws_default_security_group" "sg_default" {
  vpc_id = aws_vpc.vpc.id

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}  
