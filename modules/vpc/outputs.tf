output "vpc_id" {
  description = "Name (id) of the VPC"
  value       = aws_vpc.vpc.id
}

output "vpc_subnet" {
    value = aws_subnet.vpc_subnet  
}

output "vpc_nic" {
    value = aws_network_interface.vpc_nic.id   
}
