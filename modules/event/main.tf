resource "aws_cloudwatch_event_rule" "event" {
  name        = "Some-of-my-scheduled-event"
  description = "Starting something on schedule"

  schedule_expression = var.scheduler
}

resource "aws_cloudwatch_event_target" "target" {
  rule      = aws_cloudwatch_event_rule.event.name
  target_id = "Something_HAPPENED"
  arn       = var.target_function
}




