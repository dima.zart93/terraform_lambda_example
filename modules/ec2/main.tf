resource "aws_instance" "ec2" {
  ami           = var.OS 
  instance_type = var.instance_type
  network_interface {
    network_interface_id = var.network_interface_id
    device_index         = 0
  } 
  key_name = var.key_name

  user_data = var.user_data

  tags = var.tags
}