echo "Destroying task-environment"
cd .\dev\
terraform destroy --var-file=..\env\dev.tfvars

echo "Destroying tfstate-backend"
cd ..\backend\
terraform destroy --var-file=..\env\backend.tfvars