module "s3" {
    source = "../modules/s3"
    bucket_name = "${var.name}-${var.namespace}-bucket"
    tags = {
        Name   = var.name
        Namespace = var.namespace
    }
}  

module "dynamo" {
    source = "../modules/dynamo"
    lock_name         = "${var.name}-locks"
    tags = {
        Name   = var.name
        Namespace = var.namespace
    }
}