echo "Deploying tfstate-backend"
cd .\backend\
terraform init
terraform apply --var-file=..\env\backend.tfvars
echo "Deploying task environment"
cd ..\dev\
terraform init
terraform apply --var-file=..\env\dev.tfvars
