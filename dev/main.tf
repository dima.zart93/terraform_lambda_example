module "event" {
    source = "../modules/event"
    scheduler = var.scheduler
    target_function = module.lambda.name
}

module "sns" {
    source = "../modules/sns"
    project_name = var.project_name
    notification_protocol = var.notification_channel
    receiver = var.receiver
}

module "lambda" {
    source = "../modules/lambda"
    project_name = var.project_name
    policy = "${file("./project/policy.json")}"
    runtime = var.runtime
    lambda_trigger = module.event.name
    lambda_destination = module.sns.name
}

