terraform {
  backend "s3" {
    profile = "mycloud"
    # Replace this with your bucket name!
    bucket         = "hello-tsart-bucket"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    # Replace this with your DynamoDB table name!
    dynamodb_table = "hello-locks"
    encrypt        = true
  }
}

provider "aws" {
  profile = var.profile
  region = var.region
}