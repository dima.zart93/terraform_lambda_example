### ------- Universal variables --------- ###
variable "profile" {
  description = "Which profile to use"
  default = "mycloud"
}

variable "project_name"{
  description = "Common name of project"
  default = "training"
}

variable "namespace" {
  description = "Common namespace"
  default = "tsart"
}

variable "region" {
  description = "Region for bucket"
  default = "us-east-1"
}

variable "notification_channel" {
  default = "email"
}

variable "receiver" {
  description = "Notifications receiver"
  default = "dima.zart93@gmail.com"
}

variable "scheduler" {
  default = "rate(1 hour)"
}

variable "runtime" {
  description = "Notifications receiver"
  default = "python3.8"
}